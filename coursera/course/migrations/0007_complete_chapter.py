# Generated by Django 3.0.2 on 2020-01-09 06:45

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('course', '0006_complete'),
    ]

    operations = [
        migrations.AddField(
            model_name='complete',
            name='chapter',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='course.Chapter'),
            preserve_default=False,
        ),
    ]
