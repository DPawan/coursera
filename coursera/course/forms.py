from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import Course, Chapter, Video


class SignupForm(UserCreationForm):
    role = forms.ChoiceField(required=True, widget=forms.RadioSelect(
    attrs={'class': 'Radio'}), choices=(('instructor', 'instructor'), ('learner', 'learner'),))

    class Meta:
        model = User
        fields = ('username', 'role', 'email', 'password1', 'password2',)


class CourseForm(forms.ModelForm):
    class Meta:
        model = Course
        fields = ('name',)


class ChapterForm(forms.ModelForm):
    class Meta:
        model = Chapter
        fields = ('name', )


class VideoForm(forms.ModelForm):
    class Meta:
        model = Video
        fields = ('name', 'url',)
