from django.db import models
from django.conf import settings
from django import forms
# Create your models here.
roles = (
    ('instructor', 'instructor'),
    ('learner', 'learner'),
)


class Role(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    role = models.CharField(max_length=100, choices=roles, default='learner')


class Course(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)


class Chapter(models.Model):
    course = models.ForeignKey('Course', on_delete=models.CASCADE)
    name = models.CharField(max_length=200)


class Video(models.Model):
    chapter = models.ForeignKey('Chapter', on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    url = models.CharField(max_length=200)


class Complete(models.Model):
    video = models.ForeignKey('Video', on_delete=models.CASCADE)
    chapter = models.ForeignKey('Chapter', on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)