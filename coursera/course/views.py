from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate, logout
from course.forms import SignupForm, CourseForm, ChapterForm, VideoForm
from course.models import Role, Chapter, Course, Video, Complete
from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthenticationForm
from django.contrib import messages
from .services import save_user, login_user, save_new_course, save_new_chapter, save_new_video
# Create your views here.


def signup(request):
    if request.user.is_authenticated:
        messages.info(request, f"You are already registerd in as {request.user.username}")
        return redirect('/home')
    if request.method == 'POST':
        form = SignupForm(request.POST)
        if form.is_valid():
            save_user(form, request)
            return redirect('/home')
    else:
        form = SignupForm()
    return render(request, 'signup.html', {'form': form, 'user': request.user})


def login_request(request):
    if request.user.is_authenticated:
        messages.info(request, f"You are already logged in as {request.user.username}")
        return redirect('/home')
    if request.method == 'POST':
        form = AuthenticationForm(request=request, data=request.POST)
        if form.is_valid():
            login_user(form, request)
            return redirect('/home')
        else:
            messages.error(request, "Invalid username or password.")
    form = AuthenticationForm()
    return render(request, 'login.html', {'form': form, 'user': request.user})


def logout_request(request):
    logout(request)
    return redirect('/login')


def home(request):
    if not request.user.is_authenticated:
        return redirect('/login')
    courses = Course.objects.all()
    r = Role.objects.get(user__id=request.user.id)
    if r.role == 'instructor':
        if request.method == 'POST':
            save_new_course(request)
        form = CourseForm()
        return render(request, 'home.html', {'form': form, 'courses': courses,
                        'user': request.user})
    else:
        return render(request, 'home.html', {'courses': courses,
                                 'user': request.user})


def chapters(request, course_id):
    if not request.user.is_authenticated:
        return redirect('/login')
    course = Course.objects.get(id=course_id)
    chapters = Chapter.objects.filter(course=course).reverse()
    r = Role.objects.get(user__id=request.user.id)
    if r.role == 'instructor' and course.user == request.user:
        if request.method == 'POST':
            save_new_chapter(request, course)
        form = ChapterForm()
        return render(request, 'course.html', {'form': form, 'course': course,
                            'chapters': chapters, 'user': request.user})
    else:
        return render(request, 'course.html', {'course': course, 'chapters': chapters,
                            'user': request.user, 'complete': Complete})


def videos(request, course_id, chapter_id):
    if not request.user.is_authenticated:
        return redirect('/login')
    r = Role.objects.get(user__id=request.user.id)
    course = Course.objects.get(id=course_id)
    chapter = Chapter.objects.get(id=chapter_id)
    if r.role == 'instructor' and course.user == request.user:
        videos = Video.objects.filter(chapter=chapter)
        if request.method == 'POST':
            save_new_video(request, chapter)
        form = VideoForm()
        return render(request, 'chapter.html', {'form': form, 'chapter': chapter,
                                 'videos': videos, 'user': request.user, 'course': course})
    else:
        videos = Video.objects.filter(chapter=chapter)
        completed = Complete.objects.filter(user=request.user, chapter=chapter)
        completed_videos = []
        for complete_video in completed:
            completed_videos.append(complete_video.video)
        total_videos = videos.count()
        total_completed = completed.count()
        if total_videos == 0:
            progress = 0
        else:
            progress = (total_completed / total_videos) * 100
        return render(request, 'chapter.html', {'chapter': chapter, 'videos': videos,
                                 'user': request.user, 'course':course, 'role': 'learner', 'completed': completed_videos, 'progress': progress})


def complete(request, course_id, chapter_id, video_id):
    if not request.user.is_authenticated:
        return redirect('/login')
    # r = Role.objects.get(user__id=request.user.id)
    video = Video.objects.get(id=video_id)
    # print(r, video_id)
    if Complete.objects.filter(user=request.user, video=video).first() is None:
        Complete.objects.create(user=request.user, video=video, chapter=Chapter.objects.get(pk=chapter_id))
    return redirect('videos', course_id=course_id, chapter_id=chapter_id)
    