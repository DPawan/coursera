from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate, logout
from course.forms import SignupForm, CourseForm, ChapterForm, VideoForm
from course.models import Role, Chapter, Course, Video
from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthenticationForm
from django.contrib import messages


def save_user(form, request):
    form.save()
    username = form.cleaned_data.get('username')
    password = form.cleaned_data.get('password1')
    user = authenticate(username=username, password=password)
    role = form.cleaned_data.get('role')
    Role.objects.create(user=user, role=role)
    login(request, user)


def login_user(form, request):
    username = form.cleaned_data.get('username')
    password = form.cleaned_data.get('password')
    user = authenticate(username=username, password=password)
    if user is not None:
        login(request, user)
        messages.info(request, f"You are now logged in as {username}")
        return redirect('/home')
    else:
        messages.error(request, "Invalid username or password.")


def save_new_course(request):
    form = CourseForm(request.POST)
    if form.is_valid():
        course_name = form.cleaned_data.get('name')
        Course.objects.create(user=request.user, name=course_name)
    else:
        messages.error('Could not validate')


def save_new_chapter(request, course):
    form = ChapterForm(request.POST)
    if form.is_valid():
        chapter_name = form.cleaned_data.get('name')
        Chapter.objects.create(course=course, name=chapter_name)
    else:
        messages.error('Could not validate')


def save_new_video(request, chapter):
    form = VideoForm(request.POST)
    if form.is_valid():
        video_name = form.cleaned_data.get('name')
        video_url = form.cleaned_data.get('url')
        Video.objects.create(chapter=chapter, name=video_name, url=video_url)
    else:
        messages.error('Could not validate')
