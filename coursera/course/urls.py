from django.urls import path
from . import views

urlpatterns = [
    path('signup/', views.signup, name='signup'),
    path('login/', views.login_request, name='login'),
    path('home/', views.home, name='home'),
    path('', views.home, name='home'),
    path('logout/', views.logout_request, name='logout'),
    path('course/<int:course_id>', views.chapters, name='chapters'),
    path('course/<int:course_id>/chapter/<int:chapter_id>', views.videos, name='videos'),
    path('course/<int:course_id>/chapter/<int:chapter_id>/video/<int:video_id>', views.complete, name='complete')
]